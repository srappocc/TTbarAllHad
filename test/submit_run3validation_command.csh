# Submit the crab files 

python submit.py -c PSet.py -d WriteMistagsRun3 -f datasets/datasets_qcdmc_Run3Summer19NanoAOD.txt -t WriteMistagsRun3 --shscript crab_script_mistag.sh --nanoscript run_write_mistagrate.py -o ttbarreshad.root

# After output, get the results
python multicrab.py -c getoutput -w WriteMistagsRun3

# Add them together
python hadd_dirs.py -d WriteMistagsRun3
mv WriteMistagsRun3* hists/
# The flat QCD MC is the mod mass sample:
cp hists/WriteMistagsRun3_crab__QCD_Pt-15to7000_TuneCP5_Flat_13TeV_pythia8_RunIIFall17NanoAODv4-PU2017_12Apr2018_Nano14Dec2018_102X.root ./modmass_run3.root

#
# Next : run the ipynbs for mistags and control plots. These will create files that are used for the bkg estimate.
#
#  (This is interactive)
# 
#
# The outputs are then copied to specific files:
# 
mv mistag/mistag_run3_rates.root .


# Now run the background estimation.
python submit.py -c PSet.py -d BkgEstimateRun3 -f datasets/datasets_qcdmc_Run3Summer19NanoAOD.txt -t BkgEstimateRun3 --shscript crab_script.sh --nanoscript run_plots.py -o ttbarreshad.root
python submit.py -c PSet.py -d BkgEstimateRun3 -f datasets/datasets_signals_Run3Summer19NanoAOD.txt -t BkgEstimateRun3 --shscript crab_script.sh --nanoscript run_plots.py -o ttbarreshad.root

# Add them together
python hadd_dirs.py -d BkgEstimateRun3
mv BkgEstimateRun3_*.root hists/

