# TTbarAllHad

This is intended to replicate the ttbar all-hadronic workflow from previous iterations using NANOAOD. 

The status is:
   - Triggers (done)
   - Mistag rate (done)
   - Background estimate (done)
   - Systematic uncertainties (to do)
   - Limits (to do)


## Recipe

```
cmsrel CMSSW_10_2_9
cd CMSSW_10_2_9/src
cmsenv
git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools
git clone https://github.com/b2g-nano/TTbarAllHad.git Analysis/TTbarAllHad
git clone https://github.com/rappoccio/PredictedDistribution.git Analysis/PredictedDistribution
scram b -j 10
cd Analysis/TTbarAllHad/test
ln -s ../../../PhysicsTools/NanoAODTools/scripts/haddnano.py .
```


## Workflow

The entire workflow is outlined in [submit_command.csh](https://gitlab.cern.ch/srappocc/TTbarAllHad/blob/master/test/submit_command.csh). 